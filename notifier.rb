#!/usr/bin/ruby

require "slack-notifier"
require "yaml"
require "json"
require "http"


support = YAML.load_file("team/data/support-team.yaml")
to_notify = ENV["TO_NOTIFY"]
service_key = ENV["PD_SERVICE_KEY"]
PD_ENDPOINT = "https://api.pagerduty.com"

#frt_endpoint = "https://api.pagerduty.com/oncalls?escalation_policy_ids%5B%5D=PXMRPM3"

client = HTTP.auth("Token token=#{service_key}").headers(
				  "Content-Type" => "application/json",
				  "Accept" => "application/vnd.pagerduty+json;version=2"
  )

response = client.get("#{PD_ENDPOINT}/oncalls?escalation_policy_ids%5B%5D=PXMRPM3")
parsed = JSON.parse(response.body)
people_to_notify = []

parsed["oncalls"].each do |u|
  people_to_notify << u["user"]["id"]
end


if to_notify == nil
   to_notify = ""
end

notifier = Slack::Notifier.new ENV["WEBHOOK"]

#build notification string
users_to_notify = ""
people_to_notify.each do |p|
   slack_id = support.select{ |x| x['pagerduty']['id'] == p }.first['slack']['id']
   users_to_notify = users_to_notify + " <@#{slack_id}>"
end

notifier.ping "*New Tickets Sweep Time!* %s - check to see if there are any new tickets that'll breach in the next 8 hours, and either get replies out or assign some teammates to reply! Please also double check the `needs-org` queue and make sure all tickets have an org (this includes both Self-Managed and .com tickets!). " % users_to_notify
