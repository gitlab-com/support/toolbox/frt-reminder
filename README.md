# FRT Reminder
A simple Slack application to send reminders to sweep the queues.

## INACTIVE

As of 2021-Oct-01, this application is no longer in use. See [this issue](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/3771) for more information.

## How to setup
1. Create a new Slack application and generate a webhook for a testing channel, and a notification channel
1. Set a project-level variable `WEBHOOK` with the created webhook for your testing channel
1. Create a scheduled pipeline for each reminder. Include a variable `TO_NOTIFY` with a **comma separated** list of folks listed in `support.yml` as well as a `WEBHOOK` for the notification channel

### What if the person I'm adding isn't in `support.yml`?
To add someone new you will need two things:
- their Slack ID
- their PagerDuty ID

To find Slack ID, click the person's name in Slack, then click three dots > Copy member ID, see [How to find a Slack user ID](https://help.workast.com/hc/en-us/articles/360027461274-How-to-find-a-Slack-user-ID) for reference.

To find their PagerDuty ID, go to the [PD Users Page](https://gitlab.pagerduty.com/users) and search for their name. Their ID will be in the URL. For example in `https://gitlab.pagerduty.com/users/PQD4PZ2` the ID is `PQD4PZ2`

## How to use
Once the PagerDuty ID and Slack ID are in, there's nothing further to do. The scheduled pipeline will take care of the rest by pulling the current FRT Hawk from PagerDuty and tagging them in the Slack message.

The schedule pipelines are here:
   - [AMER](https://gitlab.com/gitlab-com/support/toolbox/frt-reminder/pipeline_schedules/32501/edit)
   - [APAC](https://gitlab.com/gitlab-com/support/toolbox/frt-reminder/pipeline_schedules/32683/edit)
   - [EMEA](https://gitlab.com/gitlab-com/support/toolbox/frt-reminder/pipeline_schedules/32684/edit) - currently disabled

### Attributions
Icon is Falcon with a Cap by Cards Against Humanity from the Noun Project
